
import MiniHttpRequest from "./MiniHttpRequest";
import HttpServerCmd from "../net/cmd/HttpServerCmd";
import { PlatformController } from "../controller/PlatformController";
import CallBackFunc from "../util/CallBackFunc";


export default class WxLogin {

    private loginUrl: string;
    private success: CallBackFunc;
    private fail: CallBackFunc;
    private wxCode: any;

    private _starteLogintTime: number;

    constructor() {

    }

    /**
    * @method
    * 进行服务器登录，以获得登录会话
    * 受限于微信的限制，本函数需要在 <button open-type="getUserInfo" bindgetuserinfo="bindGetUserInfo"></button> 的回调函数中调用
    * 需要先使用 <button> 弹窗，让用户接受授权，然后再安全调用 PlatformController.getUserInfo 获取用户信息
    *
    * @param {Object}   opts           登录配置
    * @param {string}   opts.loginUrl  登录使用的 URL，服务器应该在这个 URL 上处理登录请求，建议配合服务端 SDK 使用
    * @param {string}   [opts.method]  可选。请求使用的 HTTP 方法，默认为 GET
    * @param {Function} [opts.success] 可选。登录成功后的回调函数，参数 userInfo 微信用户信息
    * @param {Function} [opts.fail]    可选。登录失败后的回调函数，参数 error 错误信息
    */
    public login(loginUrl: string, success: CallBackFunc, fail: CallBackFunc) {
        this._starteLogintTime = new Date().getTime(); 
        this.loginUrl = loginUrl;
        this.success = success;
        this.fail = fail;
        PlatformController.wxLogin(new CallBackFunc(this, this.onWxLoginSuccess), new CallBackFunc(this, this.onWxloginFail))
    }

    private onWxLoginSuccess(res): void {
        //this.wxCode = res.code;
        this.getSetting();
    }

    private onWxloginFail(): void {
        this.fail.call(new Error("微信登录失败！！"));
    }

    private getSetting(): void {
        // if (PlatformController.hasSettingApi) {
        //     PlatformController.getSetting2({ success: this.onGetSetting });
        // }
        // else {
        this.toLogin();
        // }
    }

    private onGetSetting = (res): void => {
        if (res.authSetting['scope.userInfo']) { //如果已经授权则尝试获取unionId
            this.getUserInfo();
        }
        else { //没有授权就只获取openId
            this.toLogin();
        }
    }

    private getUserInfo(): void {
        // PlatformController.getUserInfo({
        //     withCredentials: true,
        //     success: this.getUserInfoSuccess,
        //     fail: this.getUserInfoFail
        // });
        this.getUserInfoSuccess();
    }

    private getUserInfoSuccess = (userResult?): void => {
        // if (userResult) {
        //     this.toLogin(userResult.encryptedData, userResult.iv);
        // }
        // else {
            this.toLogin();
        // }
    }

    private toLogin(encryptedData?: any, iv?: any): void {
        // 构造请求头，包含 code、encryptedData 和 iv
        let header = { "X-WX-Code": this.wxCode }
        if (encryptedData != null) {
            header["X-WX-Encrypted-Data"] = encryptedData;
            header["X-WX-IV"] = iv;
        }
        var mini: MiniHttpRequest = new MiniHttpRequest();
        mini.request(this.loginUrl, "POST", {cmdType:HttpServerCmd.ACCOUNT_LOGIN}, header);
        mini.addEventListener(Laya.Event.COMPLETE, this, this.onRequestSuccess);
        mini.addEventListener(Laya.Event.ERROR, this, this.onRequestFail);
    }

    private onRequestSuccess(result): void {
        const data = result;
        if (!data || !data.data || !data.data.secretKey) {
            return this.fail.call(new Error(`响应错误，${JSON.stringify(data)}`));
        }

        console.log("本次登录耗时:" + (new Date().getTime() - this._starteLogintTime) + "毫秒");
        this.success.call(result);
    }

    private onRequestFail(res): void {
        console.warn('登录失败，可能是网络错误或者服务器发生异常,res=', res);
        return this.fail.call(res);
    }

    private getUserInfoFail = (userResult): void => {
        return this.fail.call(new Error('获取微信用户信息失败，请检查网络状态'));
    }
}