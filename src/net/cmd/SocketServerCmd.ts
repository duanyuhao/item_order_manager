
export default class SocketServerCmd {
    static C2S_SYNC_TIME = 1;
    static S2C_LOGIN = 2;
     /**
     * 服务器返回玩家正常添加到匹配队列
     */
    static S2C_ADD_MATCH_QUEUE_SUCCESS = 10;
    /**开始匹配 */
    static S2C_START_FIGHT_MATCH = 11;
    /**玩家加载完成表示已经准备好 int32 step; // 加载阶段 1表示匹配界面资源加载完成   2表示跳伞动画资源加载完成*/
    static C2S_LOAD_FINISH_READY = 12;
    /**
     * 玩家选择出生点
     */
    static C2S_SELECT_BURN_POINT = 13;
    /**
     * 玩家跳伞加载完成表示已经准备好
     */
    static C2S_LOAD_JUMP_FINISH_READY = 14;
    /**
     * 同步帧信息
     */
    static C2S_SYNC_FRAME_INFO = 15;
    /**
     * 同步某个玩家杀死另外一个玩家
     */
    static C2S_SYNC_KILL_PLAYER = 16;

    /**
     * 游戏结束了
     */
    static S2C_GAME_OVER = 16;

    static C2S_EXIT_GAME = 17;
    /**
    * 通知玩家返回大厅 加载匹配资源失败或者跳伞资源失败
    */
    static S2C_BACK_TO_MAIN_SCENE = 17;


    /**
        * 客户端发送过来取消战斗匹配
        */
    static C2S_CANCEL_FIGHT_MATCH = 18;
    /**
    * 服务器返回取消战斗匹配成功
    */
    static S2C_CANCEL_FIGHT_MATCH = 18;
}