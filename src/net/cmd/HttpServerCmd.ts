export default class HttpServerCmd {
    //玩家账号登录
    static ACCOUNT_LOGIN = 1;
    //玩家分区登录
    static REGION_LOGIN = 2;
    /**
     * 更新每天限制的数据
     */
    static UPDATE_DAILY_LIMIT_DATA = 16;
    /**
     * 签到奖励
     */
    static GET_SIGN_REWARD = 11;
    /**
     * 同步游戏客户端数据
     */
    static SYNC_GAME_CLIENT_DATA = 17;
    /**接受邀请/更新邀请好友信息 */
    static ACCEPT_INVITATION = 27;
    /**请求领取邀请好友的奖励 */
    static GET_INVITE_REWARD = 29;
    /**获得一个单字 */
    static GET_WORD_BY_MYSELF = 40;
    /**发放索要的单字 */
    static SEND_WORD_FOR_ASK = 41;
    /**获得字信息列表 */
    static GET_WORD_DATA_ALL = 42;
    /**字兑换成体力 */
    static WORD_TO_POWER = 43;
    /**提交订单 */
    static SUBMIT_ORDER = 44;
    /**获取当天订单 */
    static GET_ORDER_LIST = 45;
    /**管理员登陆 */
    static ADMIN_LOGIN = 47;
    /**更新订单状态 */
    static SYNC_ORDER_STATUS = 46;
}