import WxLogin from "./WxLogin";
import CallBackFunc from "../util/CallBackFunc";
import MiniHttpRequest from "./MiniHttpRequest";
import EventManager from "../manager/EventManager";
import HttpServerCmd from "../net/cmd/HttpServerCmd";


export default class MiniServer {
    public static WX_HEADER_CODE: 'X-WX-Code';
    public static WX_HEADER_ENCRYPTED_DATA: 'X-WX-Encrypted-Data';
    public static WX_HEADER_IV: 'X-WX-IV';
    public static WX_HEADER_ID: 'X-WX-Id';
    public static WX_HEADER_SKEY: 'X-WX-Skey';
    public static WX_HEADER_REGION: string = 'X-WX-Region';
    public static X_WX_GAME_ID: string = 'x-wx-game-id';
    public static X_WX_OPEN_ID: string = 'x-wx-open-id';


    private _serverAddr: string;
    private _access_token: string = null;
    private _regionID: number;


    constructor() {
    }

    public get access_token(): string {
        return this._access_token;
    }

    public setup(serverAddr: string): void {
        this._serverAddr = serverAddr;
    }

    public login(): void {
        var ex: WxLogin = new WxLogin();
        ex.login(this._serverAddr, new CallBackFunc(this, this.onGetTokensuccess), new CallBackFunc(this, this.onGetTokenFail));
        
    }
    //REGION_LOGIN
    public regionLogin() {
        this.request('REGION_LOGIN', { cmdType: HttpServerCmd.REGION_LOGIN, /*userId: SdkManager.uid*/ }, null, 'post');
    }

    private onGetTokenFail(args: any = null): void {
        console.log("onGetTokenFail", args);
        //EventManager.dispatchEvent(GameEvent.SERVER_LOGIN_FAIL);
    }

    public onGetTokensuccess(tokenRes): void {
        console.log("tokenRes:", tokenRes);
        var data = tokenRes;
        if (data.code == 0) {
            var res = data.data;
            // GameDataManager.isNewUser = res.newUser == 1;
            // GameDataManager.openId = res.openId;
            this._regionID = res.regionId;
            this._access_token = res.secretKey;
            // SdkManager.login();
            this.regionLogin();
            // EventManager.dispatchEvent(GameEvent.LOGIN_SUCCESS, data.data);
        } else {
            //EventManager.dispatchEvent(GameEvent.SERVER_LOGIN_FAIL);
        }
    }

    public updateServerInfo(res){
        this._regionID = res.regionId;
        this._access_token = res.secretKey;
    }

    public get isLoginSuccess(): boolean {
        return this._access_token != null;
    }

    public request(api: any, data: any = null, isNeedRegion: boolean = true, method: string = "", heads: any = null): void {
        // if (!this.isLoginSuccess) {
        //     return;
        // }

        var mini: MiniHttpRequest = new MiniHttpRequest();
        var url: string;
        if (isNeedRegion && this._regionID && this._regionID > 0) {
            url = this._serverAddr + "/s" + this._regionID;
        }
        else {
            url = this._serverAddr;
        }
        if ((typeof api === "string") && api.indexOf("https:") != -1) {
            url = api;
        }
        if (!method) {
            method = "GET";
            if (data) {
                method = "POST";
            }
        }
        if (heads == null) {
            heads = { "X-WX-Skey": this._access_token };
        } else {
            heads["X-WX-Skey"] = this._access_token;
        }

        if (isNeedRegion) {
            //多服务器ID区分需要
            heads[MiniServer.WX_HEADER_REGION] = this._regionID;
        }

        mini.request(url, method, data, heads);
        mini.on(Laya.Event.COMPLETE, this, this.onReqSuccess, [api]);
        mini.on(Laya.Event.ERROR, this, this.onReqError, [api]);
    }

    private onReqSuccess(api: string, response: any): void {
        if (response) {
            console.log('response', response);
            EventManager.dispatchEvent(api, response);
            // if (response.code === 0 && response.data) {
            //     let data = response.data;
            //     console.log('api', api, 'response', data);
            //     EventManager.dispatchEvent(api, data);
            // }
            // else if (response.code == -2)//服务器SKEY过期
            // {
            //     //EventManager.dispatchEvent(GameEvent.SERVER_SKEY_EXPIRE);
            // }
        }
    }

    private onReqError(api: string, response: any): void {
        if (response) {
            // EventManager.dispatchEvent(api, response);
            if (response.code == -2)//服务器SKEY过期
            {
                //EventManager.dispatchEvent(GameEvent.SERVER_SKEY_EXPIRE);
            }
        }
    }
}