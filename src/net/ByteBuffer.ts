/*
* 自定义byte
*/
export default class ByteBuffer extends Laya.Byte {

	constructor(endianType: string = "") {
		super();
		if (endianType == "" || endianType == Laya.Byte.BIG_ENDIAN) {
			this.endian = Laya.Byte.BIG_ENDIAN;
		}
		else {
			this.endian = Laya.Byte.LITTLE_ENDIAN;
		}
	}

	public writeVarint32(value: number): void {
		while (true) {
			if ((value & ~0x7F) == 0) {
				this.writeByte(value);
				return;
			}
			else {
				this.writeByte((value & 0x7F) | 0x80);
				value >>>= 7;
			}
		}
	}

	public readVarint32(): number {
		return (this.readUint32()) | 0;
	}

	public readUint32(): number {
		var result: number = 0
		for (var i: number = 0; ; i += 7) {
			var b: number = this.readUint8();
			if (i < 32) {
				if (b >= 0x80) {
					result |= ((b & 0x7f) << i)
				}
				else {
					result |= (b << i)
					break;
				}
			}
			else {
				while (this.readUint8() >= 0x80) {
				}
				break;
			}
		}

		return result;
	}

	public writeUint32(value: number): void {
		for (; ;) {
			if (value < 0x80) {
				this.writeByte(value)
				return;
			} else {
				this.writeByte((value & 0x7F) | 0x80)
				value >>>= 7
			}
		}
	}
}