import MiniServer from "./MiniServer";

export default class MiNi {
    constructor() {

    }

    public static _miniHttpSerer: MiniServer;

    public static setup(): void  {
        this._miniHttpSerer = new MiniServer();
        this._miniHttpSerer.setup('https://testcyorange.haidian666.com/minGame/cmd');
    }

    public static login():void{
        if(!this._miniHttpSerer){
            console.error("Mini HTTP Server 尚未初始化");
            return;
        }
        this._miniHttpSerer.login();
    }

    //REGION_LOGIN
    public static regionLogin(){
        this._miniHttpSerer.regionLogin();
    }

    public static request(api: any, data: any = null, isNeedRegion: boolean = true, method: string = "", heads: any = null):void {
        if(!this._miniHttpSerer){
            console.error("Mini HTTP Server 尚未初始化");
            return;
        }
        this._miniHttpSerer.request(api, data, isNeedRegion, method, heads);
    }
}