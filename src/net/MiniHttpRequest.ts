import EventDispatcher from "../manager/EventDispatcher";

/*
* 封装了wx的request 和laya的HttpRequest ，
* 使上层时候不用关心当前是需要使用PlatformController.request 还是laya的HttpRequest
*/
export default class MiniHttpRequest extends EventDispatcher {
    private _startTime: number;
    private _url: string;

    constructor() {
        super();
    }

    /**
     * 发送服务器请求
     * @param url 
     * @param method 
     * @param data 
     * @param header 
     */
    public request(url: string, method: string, data?: any, header?: any): void {
        this._startTime = new Date().getTime();
        this._url = url;
        console.log("HTTP请求服务器:" + url + "参数=", data);
        var str: string = "";
        if (data instanceof Object) {
            // for (var key in data) {
            //     str += key + "=" + data[key] + "&"
            // }
            data = JSON.stringify(data);
        }
        if (str.length > 0) {
            str = str.substr(0, str.length - 1);
            data = str;
        }


        var ht: Laya.HttpRequest = new Laya.HttpRequest();
        ht.on(Laya.Event.COMPLETE, this, this.onSuccess);
        ht.on(Laya.Event.ERROR, this, this.onFail);
        if (header) {
            var haedArr: Array<any> = new Array<any>();
            for (var key in header) {
                haedArr.push(key);
                haedArr.push(header[key]);
            }
            ht.send(url, data, method, "json", haedArr);
        } else {
            ht.send(url, data, method, "json");
        }

    }

    private onSuccess = (res) => {
        console.log("HTTP请求服务器返回:", res);
        console.log("请求耗时:", new Date().getTime() - this._startTime, this._url);
        res = res.data;
        this.event(Laya.Event.COMPLETE, res)
    }

    private onFail = (res) => {
        // console.warn("onHttpFail", JSON.stringify(res));
        this.event(Laya.Event.ERROR, res);
    }
}