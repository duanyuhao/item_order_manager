/**This class is automatically generated by LayaAirIDE, please do not make any modifications. */
import View=Laya.View;
import Dialog=Laya.Dialog;
import Scene=Laya.Scene;
var REG: Function = Laya.ClassUtils.regClass;
export module ui.test {
    export class NoticeViewUI extends Laya.Scene {
		public box:Laya.Box;
		public bg:Laya.Image;
		public text:Laya.Label;
        constructor(){ super()}
        createChildren():void {
            super.createChildren();
            this.loadScene("test/NoticeView");
        }
    }
    REG("ui.test.NoticeViewUI",NoticeViewUI);
    export class OrderItemViewUI extends Laya.Scene {
		public playername:Laya.Label;
		public address:Laya.Label;
		public phone:Laya.Label;
		public idnum:Laya.Label;
		public confer:Laya.Button;
		public submittime:Laya.Label;
		public logincnt:Laya.Label;
		public invitecnt:Laya.Label;
		public exchangecnt:Laya.Label;
		public statusstr:Laya.Label;
		public status:Laya.RadioGroup;
        constructor(){ super()}
        createChildren():void {
            super.createChildren();
            this.loadScene("test/OrderItemView");
        }
    }
    REG("ui.test.OrderItemViewUI",OrderItemViewUI);
    export class OrderListViewUI extends Laya.Dialog {
		public box:Laya.Box;
		public list:Laya.List;
		public search:Laya.TextInput;
		public btn_search:Laya.Image;
		public searchdate:Laya.TextInput;
		public btn_selectdate:Laya.Image;
		public nodata:Laya.Label;
        constructor(){ super()}
        createChildren():void {
            super.createChildren();
            this.loadScene("test/OrderListView");
        }
    }
    REG("ui.test.OrderListViewUI",OrderListViewUI);
    export class TestSceneUI extends Laya.Scene {
		public login:Laya.Sprite;
		public username:Laya.TextInput;
		public password:Laya.TextInput;
        constructor(){ super()}
        createChildren():void {
            super.createChildren();
            this.loadScene("test/TestScene");
        }
    }
    REG("ui.test.TestSceneUI",TestSceneUI);
}