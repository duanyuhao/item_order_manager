
import EventManager from "../manager/EventManager";
import CallBackFunc from "../util/CallBackFunc";
export class PlatformController {

    public static wxLogin(loginSuccess: CallBackFunc, loginFailCall: CallBackFunc): void {
            console.log("开始微信登录！")
            var wxLoginSuccess = (res) => {
                console.log("微信登录成功，");
                loginSuccess.call(res)
            }
            var wxLoginFail = (res) => {
                console.error("微信登录失败~！", res.errMsg);
                loginFailCall.call();
            }
            loginSuccess.call();
    }
}
