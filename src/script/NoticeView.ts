import { ui } from "../ui/layaMaxUI";


export class NoticeView extends ui.test.NoticeViewUI{
    private static _instance: NoticeView;

    public static get ins(): NoticeView {
        if (this._instance == null) {
            this._instance = new NoticeView();
        }
        return this._instance;
    }

    private constructor() {
        super();
        this.x = (Laya.stage.width - this.width) >> 1;
        this.y = ((Laya.stage.height - this.height) >> 1) + 100;
        this.zOrder = 1100;//显示在dialog上面
    }

    public show(text: string) {
        this.text.text = text;
        var width: number = Math.max(this.text.displayWidth + 40, 400);
        // this.text.width = this.text.displayWidth;
        this.text.x = (this.width - this.text.displayWidth) >> 1;
        this.bg.width = width;
        this.bg.x = (this.width - width) >> 1;
        Laya.stage.addChild(this);
        Laya.timer.once(1500, this, this.hide);

        this.box.y = 0;
        Laya.Tween.to(this.box, { y: -50 }, 1000, null, null, 500, true);
    }

    private hide() {
        this.removeSelf();
    }
}