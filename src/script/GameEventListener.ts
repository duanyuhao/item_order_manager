import EventManager from "../manager/EventManager";
import OrderListView from "./OrderListView";

export default class GameEventListener {
    
    public static init(){
        EventManager.addEvent('REGION_LOGIN', GameEventListener, GameEventListener.showListView);
    }

    private static showListView(){
        OrderListView.ins.show();
    }
}