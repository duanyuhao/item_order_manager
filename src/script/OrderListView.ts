import { ui } from "../ui/layaMaxUI";
import OrderItemRender from "./OrderItemRender";
import OrderInfo from "./OrderList";
import MiNi from "../net/MiNi";
import HttpServerCmd from "../net/cmd/HttpServerCmd";
import EventManager from "../manager/EventManager";
import OrderList from "./OrderList";
import MsgLockManager from "../manager/MsgLockManager";

export default class OrderListView extends ui.test.OrderListViewUI {
    
    constructor() { 
        super();
        this.isModal = true;
        this.list.itemRender = OrderItemRender;
        this.list.vScrollBarSkin = '';
        this.list.renderHandler = new Laya.Handler(this, this.updateItem);
        this.list.spaceY = 10;
        this.list.scrollBar.elasticBackTime = 200;
        this.list.scrollBar.elasticDistance = 50; 
        this.btn_search.on(Laya.Event.CLICK, this, this.onSearch);
        this.btn_selectdate.on(Laya.Event.CLICK, this, this.onSelectDate);
    }

    private static _ins: OrderListView;
    public orderList: Array<OrderInfo>;
    public static get ins(){
        if (!this._ins){
            this._ins = new OrderListView();
        }
        return this._ins;
    }
    
    onEnable(): void {
        this.updateUI();
        EventManager.addEvent('GET_ORDER_LIST', this, this.updateUI);
        MiNi.request('GET_ORDER_LIST', {cmdType: HttpServerCmd.GET_ORDER_LIST, openId: null, status: 0});
    }

    onDisable(): void {
        EventManager.removeEvent('GET_ORDER_LIST', this, this.updateUI);
    }

    updateItem(cell: OrderItemRender, index: number){
        cell.updateUi(this.orderList[index]);
    }

    updateUI(res?){
        if (res && res.orderList){
            this.orderList = [];
            for (let data of res.orderList){
                let info = new OrderList();
                info.setdata(data);
                this.orderList.push(info);
            }
            this.list.array = res.orderList;
            if (this.orderList.length > 0){
                this.searchdate.text = this.orderList[0].submitDate;
            } 
        } else {
            this.list.array = [];
        }
        this.nodata.visible = this.list.array.length === 0;
    }

    onSearch(){
        if (this.search.text){
            for (let i = 0; i < this.orderList.length; i++){
                if (this.orderList[i].orderId === this.search.text || this.orderList[i].submitDate === this.search.text){
                    this.list.scrollTo(i);
                    return;
                }
            }
        }
    }

    onSelectDate(){
        if (this.searchdate.text){
            if (!MsgLockManager.isLock('GET_ORDER_LIST')){
                MiNi.request('GET_ORDER_LIST', {cmdType: HttpServerCmd.GET_ORDER_LIST, openId: null, status: 0, date: this.searchdate.text});
                MsgLockManager.addLock('GET_ORDER_LIST', 200);
            }
        }
    }
}