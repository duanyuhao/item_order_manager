import TimeUtil from "../util/TimeUtil";

export default class OrderInfo {
    orderId: string;
    realName: string;
    phone: number;
    area: string;
    address: string;
    status: number;
    inviteCnt: number;
    loginCnt: number;
    exchangeCnt: number;
    submitTime: number;
    _submitDate: string;
    public get submitDate(){
        if (!this._submitDate){
            this._submitDate = TimeUtil.getTimeStr(this.submitTime);
        }
        return this._submitDate;
    }

    setdata(data){
        for (const key in data) {
            if (data.hasOwnProperty(key)) {
                this[key] = data[key];
            }
        }
    }
}