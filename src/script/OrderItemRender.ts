import { ui } from "../ui/layaMaxUI";
import OrderInfo from "./OrderList";
import EventManager from "../manager/EventManager";
import MiNi from "../net/MiNi";
import HttpServerCmd from "../net/cmd/HttpServerCmd";
import { NoticeView } from "./NoticeView";
import TimeUtil from "../util/TimeUtil";
import MsgLockManager from "../manager/MsgLockManager";

export default class OrderItemRender extends ui.test.OrderItemViewUI {

    constructor() {
        super();
        this.confer.on(Laya.Event.CLICK, this, this.onConfer);
    }

    onEnable(): void {
        if (!EventManager.hasEvent('SYNC_ORDER_STATUS', this, this.updateStatus)){
            EventManager.addEvent('SYNC_ORDER_STATUS', this, this.updateStatus);
        }
    }

    onDisable(): void {
        EventManager.removeEvent('SYNC_ORDER_STATUS', this, this.updateStatus)
    }

    updateUi(data: OrderInfo) {
        this.playername.text = data.realName;
        this.address.text = data.area + data.address;
        this.phone.text = data.phone + '';
        this.idnum.text = data.orderId ? data.orderId : '无';
        this.status.selectedIndex = data.status - 1;
        this.logincnt.text = data.loginCnt + '';
        this.exchangecnt.text = data.exchangeCnt + '';
        this.invitecnt.text = data.inviteCnt + '';
        this.submittime.text = TimeUtil.getTimeStr(data.submitTime);
        this.updateStatusString(data.status);
    }

    updateStatusString(status: number){
        switch (status) {
            case 1:
                this.statusstr.text = '审核中';
                this.statusstr.color = '#ec3714';
                break;
            case 2:
                this.statusstr.text = '发货中';
                this.statusstr.color = '#14ef0b';
                break;
            case 3:
                this.statusstr.text = '已发货';
                this.statusstr.color = '#14ef0b';
                break;
            case 4:
                this.statusstr.text = '不通过';
                this.statusstr.color = '#ec3714';
                break;
        }
    }

    onConfer() {
        if (!MsgLockManager.isLock('SYNC_ORDER_STATUS')){
            MiNi.request('SYNC_ORDER_STATUS', { cmdType: HttpServerCmd.SYNC_ORDER_STATUS, orderId: this.idnum.text, status: this.status.selectedIndex + 1 });
            MsgLockManager.addLock('SYNC_ORDER_STATUS', 200);
        }
    }

    updateStatus(res) {
        //console.trace('res................', res);
        if (res && res.orderInfo && res.orderInfo.orderId === this.idnum.text) {
            if (res.orderInfo.status === this.status.selectedIndex + 1){
                NoticeView.ins.show('修改成功');
                this.updateStatusString(this.status.selectedIndex + 1);
            } else {
                NoticeView.ins.show('修改失败');
            }
        } 
    }
}