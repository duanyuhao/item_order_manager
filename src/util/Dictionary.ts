
/**
* <code>Dictionary</code> 是一个字典型的数据存取类。
*/
export class Dictionary {

    private _keyValues: Object;

    constructor() {
        this._keyValues = {};
    }
    /**
     * 给指定的键名设置值。
     * @param	key 键名。
     * @param	value 值。
     */
    set(key: any, value: any): void {
        this._keyValues[key] = value;
    }

    /**
     * 返回指定键名的值。
     * @param	key 键名对象。
     * @return 指定键名的值。
     */
    get(key: any): any {
        return this._keyValues[key];
    }
    /**
     * 移除指定键名的值。
     * @param	key 键名对象。
     * @return 是否成功移除。
     */
    remove(key: any): boolean {
        this._keyValues[key] = null;
        return true;
    }
    /**
     * 清除此对象的键名列表和键值列表。
     */
    clear(): void {
        this._keyValues = {};
    }

    get keys():Array<any>{
        let ary = [];
        for(var key in this._keyValues) {
            ary.push(key);
        }
        return ary;
    }
}

