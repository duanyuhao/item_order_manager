
export default class TimeUtil {

    /**获取当前时间的字符串：如：1988-1-1 */
    public static getTimeStr(time?: number): string {
        var data: Date;
        if (time){
            data = new Date(time);
        }else {
            data = new Date();
        }
        let month = Number(data.getMonth()) + 1;
        let str1 = month >= 10 ? month + '' : '0' + month;
        let date = data.getDate();
        let str2 = date >= 10 ? date + '' : '0' + date;
        return data.getFullYear() + "-" + str1 + "-" + str2;
    }

    /**   
     *转换日期对象为日期字符串,例如：yyyy-MM-dd hh:mm:ss
      y:年，M:月份，d：天，h：小时，m:分钟，s:秒 S:毫秒
     */
    public static formatTime(time: Date, fmt: string): string {
        var o = {
            "M+": time.getMonth() + 1,
            "d+": time.getDate(),
            "h+": time.getHours(),
            "m+": time.getMinutes(),
            "s+": time.getSeconds(),
            "q+": Math.floor((time.getMonth() + 3) / 3),
            "S": time.getMilliseconds()
        };
        if (/(y+)/.test(fmt))
            fmt = fmt.replace(RegExp.$1, (time.getFullYear() + "").substr(4 - RegExp.$1.length));
        for (var k in o)
            if (new RegExp("(" + k + ")").test(fmt))
                fmt = fmt.replace(RegExp.$1, (RegExp.$1.length == 1) ? (o[k]) : (("00" + o[k]).substr(("" + o[k]).length)));
        return fmt;
    }

    /**获得 00:00:00 的字符串，小于一小时会显示成 00:00, a:秒*/
    public static getSecondTimeFomate(a: number): string {
        var isShowHour: boolean = true;
        if (a < 3600) {
            isShowHour = false;
        }
        var h: string = Math.floor(a / 3600) + '';
        if (h.length == 1) {
            h = "0" + h;
        }
        var m: string = Math.floor((a % 3600) / 60) + '';
        if (m.length == 1) {
            m = "0" + m;
        }
        var s: string = Math.floor(a % 60) + '';
        if (s.length == 1) {
            s = "0" + s;
        }
        if (isShowHour) {
            return (h + ":" + m + ":" + s);
        }
        else {
            return (m + ":" + s);
        }
    }

    /** 获取中文描述的时间 格式：20小时23分50秒  (time:秒)*/
    public static getTimeStrFomate(time: number): string {
        var str: string = "";
        var h: number = Math.floor(time / 3600);
        if (h > 0) {
            str += h + "小时";
        }
        var m: number = Math.floor((time % 3600) / 60);
        if (m > 0) {
            str += m + "分";
        }
        str += Math.floor(time % 60) + "秒";
        return str;
    }

    /** 获取描述时间 格式：20:23:50  (time:秒)*/
    public static getTimeFomate(time: number): string {
        var str: string = "";
        var h: number = Math.floor(time / 3600);
        if (h > 0) {
            str += h + ":";
        }
        var m: number = Math.floor((time % 3600) / 60);
        if (m > 0) {
            str += m + ":";
        }
        str += Math.floor(time % 60);
        return str;
    }
    /**
     * 是否过了1天
     * @param curTime 当前时间 
     * @param lastTime 上次时间
     */
    public static checkIfPass1Day(curTime: Date, lastTime: Date): boolean {
        if (curTime <= lastTime) {
            return false;
        }

        if (curTime.getFullYear() > lastTime.getFullYear()) {
            return true
        } else if (curTime.getMonth() > lastTime.getMonth()) {
            return true
        } else if (curTime.getDate() > lastTime.getDate()) {
            return true;
        }
        return false
    }
}