
export default class CallBackFunc {
    private thisArgs: any;
    private func: Function;
    private args: Array<any>;

    constructor(thisArgs: any, func: Function, ...args)  {
        this.thisArgs = thisArgs;
        this.func = func;
        this.args = args;
    }

    public call(...calArgs): void  {
        if (this.func)  {
            var par: Array<any> = new Array<any>();
            if (calArgs) {
                for (let j = 0; j < calArgs.length; ++j) {
                    par.push(calArgs[j]);
                }
            }
            if (this.args) {
                for (let j = 0; j < this.args.length; ++j) {
                    par.push(this.args[j]);
                }
            }
            this.func.apply(this.thisArgs, par);
        }
    }
}