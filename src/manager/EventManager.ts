
/**
 * 事件管理器
 * 
 */
export default class EventManager {
	private static _evtMap: Object = {};

	constructor() {
		throw new Event("静态类");
	}

	/**
	 * 发送事件(Event) 
	 * @param e
	 */
	public static dispatchEvent(type: any, ...args): void {
		var listenerList: any[] = EventManager._evtMap[type];
		if (listenerList) {
			var list: any[] = listenerList.concat();
			var i: number = 0;
			for (i = 0; i < list.length; i++) {
				var obj: any = list[i];
				var fun: Function = obj.fun;
				var fArgs: any[] = obj.args || [];
				var caller: any = obj.caller;
				if (fun != null) {
					var funArgs: any[] = [];
					if (fArgs && fArgs.length) {
						funArgs = funArgs.concat(fArgs);
					}
					if (args && args.length) {
						funArgs = funArgs.concat(args);
					}
					if (funArgs && funArgs.length) {
						fun.apply(caller, funArgs);
					}
					else {
						fun.apply(caller);
					}
				}
			}
		}
	}

	/**
	 * 添加事件侦听 
	 * @param type
	 * @param listener
	 * 
	 */
	public static addEvent(type: any, caller: any, listener: Function, ...args): void {
		if (!EventManager.hasEvent(type, caller, listener))//先查查是否已经侦听了
		{
			var listenerList: any[] = EventManager._evtMap[type];
			if (!listenerList) {
				listenerList = new Array();
				EventManager._evtMap[type] = listenerList;
			}
			listenerList[listenerList.length] = { fun: listener, args: args, caller: caller };
		}
		else {
			console.error("重复注册相同事件:" + type + ",返回!");
		}
	}

	/**
	 * 移除事件侦听 
	 * @param type
	 * @param listener
	 * 
	 */
	public static removeEvent(type: any, caller:any, listener: Function): void {
		var listenerList: any[] = EventManager._evtMap[type];
		if (listenerList) {
			var len: number = listenerList.length;
			for (var i: number = len - 1; i >= 0; i--) {
				var obj: any = listenerList[i];
				var fun: Function = obj.fun;
				if (listener == fun && obj.caller == caller) {
					listenerList.splice(i, 1);
				}
			}
			if (listenerList.length == 0) {
				delete EventManager._evtMap[type];
			}
		}
	}

	/**
	 * 是否已经侦听了某类型事件 
	 * @param type
	 * @return 
	 * 
	 */
	public static hasTypeEvent(type: any): boolean {
		return EventManager._evtMap.hasOwnProperty(type);
	}

	/**
	 * 是否已经侦听了某类型某个事件 
	 * @param type
	 * @return 
	 * 
	 */
	public static hasEvent(type: any, caller: any, listener: Function): boolean {
		var listenerList: any[] = EventManager._evtMap[type];
		if (listenerList) {
			var len: number = listenerList.length;
			for (var i: number = len - 1; i >= 0; i--) {
				var obj: any = listenerList[i];
				var fun: Function = obj.fun;
				if (listener == fun && obj.caller == caller) {
					return true;
				}
			}
		}
		return false;
	}


	/**
	 * 删除所有的事件监听
	 * 
	 */
	public static removeAllEvents(): void {
		EventManager._evtMap = {};
	}

	/**
	 * 返回同一类型事件列表
	 * @param type
	 * @return 
	 * 
	 */
	public static getTypeEventList(type: any): Function[] {
		var funList: Array<Function> = new Array<Function>();
		var listenerList: any[] = EventManager._evtMap[type];
		if (listenerList) {
			var len: number = listenerList.length;
			for (var i: number = len - 1; i >= 0; i--) {
				var obj: any = listenerList[i];
				var fun: Function = obj.fun;
				funList.push(fun);
			}
		}
		return funList;
	}

	/**
	 * 返回同一类型事件列表
	 * @param type
	 * @return 
	 * 
	 */
	public static getTypeEventsNum(type: any): number {
		var list: any[] = EventManager._evtMap[type];
		return list ? list.length : 0;
	}
}