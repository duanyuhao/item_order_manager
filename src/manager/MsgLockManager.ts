import { Dictionary } from "../util/Dictionary";

export default class MsgLockManager{
    private static _dic:Dictionary = new Dictionary();

    public static isLock(msg:any):boolean{
        return this._dic.get(msg);
    }

    public static addLock(msg:any, time:number):void{
        this._dic.set(msg,msg);
        Laya.timer.once(time, this, function(){MsgLockManager._dic.remove(msg)},null,true);
    }
}