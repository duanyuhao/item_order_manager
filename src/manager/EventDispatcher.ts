
/**
 * <code>EventDispatcher</code> 类是可调度事件的所有类的基类。
 */
export default class EventDispatcher extends Laya.EventDispatcher  {
    constructor() {
        super();
    }

    public addEventListener(type: string, caller: any, listener: Function, args?: Array<any>): Laya.EventDispatcher  {
        return super.on(type, caller, listener, args);
    }

    public removeEventListener(type: string, caller: any, listener: Function, onceOnly?: boolean): Laya.EventDispatcher  {
        return super.off(type, caller, listener, onceOnly);
    }

    public hasEventListener(type: string): boolean  {
        return super.hasListener(type);
    }

    /**
     * 派发事件。
     * @param type	事件类型。
     * @param data	（可选）回调数据。<b>注意：</b>如果是需要传递多个参数 p1,p2,p3,...可以使用数组结构如：[p1,p2,p3,...] ；如果需要回调单个参数 p ，且 p 是一个数组，则需要使用结构如：[p]，其他的单个参数 p ，可以直接传入参数 p。
     * @return 此事件类型是否有侦听者，如果有侦听者则值为 true，否则值为 false。
     */
    public dispatchEvent(type: string, data?: any): boolean  {
        return super.event(type, data);
    }
}