(function () {
    'use strict';

    var REG = Laya.ClassUtils.regClass;
    var ui;
    (function (ui) {
        var test;
        (function (test) {
            class NoticeViewUI extends Laya.Scene {
                constructor() { super(); }
                createChildren() {
                    super.createChildren();
                    this.loadScene("test/NoticeView");
                }
            }
            test.NoticeViewUI = NoticeViewUI;
            REG("ui.test.NoticeViewUI", NoticeViewUI);
            class OrderItemViewUI extends Laya.Scene {
                constructor() { super(); }
                createChildren() {
                    super.createChildren();
                    this.loadScene("test/OrderItemView");
                }
            }
            test.OrderItemViewUI = OrderItemViewUI;
            REG("ui.test.OrderItemViewUI", OrderItemViewUI);
            class OrderListViewUI extends Laya.Dialog {
                constructor() { super(); }
                createChildren() {
                    super.createChildren();
                    this.loadScene("test/OrderListView");
                }
            }
            test.OrderListViewUI = OrderListViewUI;
            REG("ui.test.OrderListViewUI", OrderListViewUI);
            class TestSceneUI extends Laya.Scene {
                constructor() { super(); }
                createChildren() {
                    super.createChildren();
                    this.loadScene("test/TestScene");
                }
            }
            test.TestSceneUI = TestSceneUI;
            REG("ui.test.TestSceneUI", TestSceneUI);
        })(test = ui.test || (ui.test = {}));
    })(ui || (ui = {}));

    class EventDispatcher extends Laya.EventDispatcher {
        constructor() {
            super();
        }
        addEventListener(type, caller, listener, args) {
            return super.on(type, caller, listener, args);
        }
        removeEventListener(type, caller, listener, onceOnly) {
            return super.off(type, caller, listener, onceOnly);
        }
        hasEventListener(type) {
            return super.hasListener(type);
        }
        dispatchEvent(type, data) {
            return super.event(type, data);
        }
    }

    class MiniHttpRequest extends EventDispatcher {
        constructor() {
            super();
            this.onSuccess = (res) => {
                console.log("HTTP请求服务器返回:", res);
                console.log("请求耗时:", new Date().getTime() - this._startTime, this._url);
                res = res.data;
                this.event(Laya.Event.COMPLETE, res);
            };
            this.onFail = (res) => {
                this.event(Laya.Event.ERROR, res);
            };
        }
        request(url, method, data, header) {
            this._startTime = new Date().getTime();
            this._url = url;
            console.log("HTTP请求服务器:" + url + "参数=", data);
            var str = "";
            if (data instanceof Object) {
                data = JSON.stringify(data);
            }
            if (str.length > 0) {
                str = str.substr(0, str.length - 1);
                data = str;
            }
            var ht = new Laya.HttpRequest();
            ht.on(Laya.Event.COMPLETE, this, this.onSuccess);
            ht.on(Laya.Event.ERROR, this, this.onFail);
            if (header) {
                var haedArr = new Array();
                for (var key in header) {
                    haedArr.push(key);
                    haedArr.push(header[key]);
                }
                ht.send(url, data, method, "json", haedArr);
            }
            else {
                ht.send(url, data, method, "json");
            }
        }
    }

    class HttpServerCmd {
    }
    HttpServerCmd.ACCOUNT_LOGIN = 1;
    HttpServerCmd.REGION_LOGIN = 2;
    HttpServerCmd.UPDATE_DAILY_LIMIT_DATA = 16;
    HttpServerCmd.GET_SIGN_REWARD = 11;
    HttpServerCmd.SYNC_GAME_CLIENT_DATA = 17;
    HttpServerCmd.ACCEPT_INVITATION = 27;
    HttpServerCmd.GET_INVITE_REWARD = 29;
    HttpServerCmd.GET_WORD_BY_MYSELF = 40;
    HttpServerCmd.SEND_WORD_FOR_ASK = 41;
    HttpServerCmd.GET_WORD_DATA_ALL = 42;
    HttpServerCmd.WORD_TO_POWER = 43;
    HttpServerCmd.SUBMIT_ORDER = 44;
    HttpServerCmd.GET_ORDER_LIST = 45;
    HttpServerCmd.ADMIN_LOGIN = 47;
    HttpServerCmd.SYNC_ORDER_STATUS = 46;

    class PlatformController {
        static wxLogin(loginSuccess, loginFailCall) {
            console.log("开始微信登录！");
            loginSuccess.call();
        }
    }

    class CallBackFunc {
        constructor(thisArgs, func, ...args) {
            this.thisArgs = thisArgs;
            this.func = func;
            this.args = args;
        }
        call(...calArgs) {
            if (this.func) {
                var par = new Array();
                if (calArgs) {
                    for (let j = 0; j < calArgs.length; ++j) {
                        par.push(calArgs[j]);
                    }
                }
                if (this.args) {
                    for (let j = 0; j < this.args.length; ++j) {
                        par.push(this.args[j]);
                    }
                }
                this.func.apply(this.thisArgs, par);
            }
        }
    }

    class WxLogin {
        constructor() {
            this.onGetSetting = (res) => {
                if (res.authSetting['scope.userInfo']) {
                    this.getUserInfo();
                }
                else {
                    this.toLogin();
                }
            };
            this.getUserInfoSuccess = (userResult) => {
                this.toLogin();
            };
            this.getUserInfoFail = (userResult) => {
                return this.fail.call(new Error('获取微信用户信息失败，请检查网络状态'));
            };
        }
        login(loginUrl, success, fail) {
            this._starteLogintTime = new Date().getTime();
            this.loginUrl = loginUrl;
            this.success = success;
            this.fail = fail;
            PlatformController.wxLogin(new CallBackFunc(this, this.onWxLoginSuccess), new CallBackFunc(this, this.onWxloginFail));
        }
        onWxLoginSuccess(res) {
            this.getSetting();
        }
        onWxloginFail() {
            this.fail.call(new Error("微信登录失败！！"));
        }
        getSetting() {
            this.toLogin();
        }
        getUserInfo() {
            this.getUserInfoSuccess();
        }
        toLogin(encryptedData, iv) {
            let header = { "X-WX-Code": this.wxCode };
            if (encryptedData != null) {
                header["X-WX-Encrypted-Data"] = encryptedData;
                header["X-WX-IV"] = iv;
            }
            var mini = new MiniHttpRequest();
            mini.request(this.loginUrl, "POST", { cmdType: HttpServerCmd.ACCOUNT_LOGIN }, header);
            mini.addEventListener(Laya.Event.COMPLETE, this, this.onRequestSuccess);
            mini.addEventListener(Laya.Event.ERROR, this, this.onRequestFail);
        }
        onRequestSuccess(result) {
            const data = result;
            if (!data || !data.data || !data.data.secretKey) {
                return this.fail.call(new Error(`响应错误，${JSON.stringify(data)}`));
            }
            console.log("本次登录耗时:" + (new Date().getTime() - this._starteLogintTime) + "毫秒");
            this.success.call(result);
        }
        onRequestFail(res) {
            console.warn('登录失败，可能是网络错误或者服务器发生异常,res=', res);
            return this.fail.call(res);
        }
    }

    class EventManager {
        constructor() {
            throw new Event("静态类");
        }
        static dispatchEvent(type, ...args) {
            var listenerList = EventManager._evtMap[type];
            if (listenerList) {
                var list = listenerList.concat();
                var i = 0;
                for (i = 0; i < list.length; i++) {
                    var obj = list[i];
                    var fun = obj.fun;
                    var fArgs = obj.args || [];
                    var caller = obj.caller;
                    if (fun != null) {
                        var funArgs = [];
                        if (fArgs && fArgs.length) {
                            funArgs = funArgs.concat(fArgs);
                        }
                        if (args && args.length) {
                            funArgs = funArgs.concat(args);
                        }
                        if (funArgs && funArgs.length) {
                            fun.apply(caller, funArgs);
                        }
                        else {
                            fun.apply(caller);
                        }
                    }
                }
            }
        }
        static addEvent(type, caller, listener, ...args) {
            if (!EventManager.hasEvent(type, caller, listener)) {
                var listenerList = EventManager._evtMap[type];
                if (!listenerList) {
                    listenerList = new Array();
                    EventManager._evtMap[type] = listenerList;
                }
                listenerList[listenerList.length] = { fun: listener, args: args, caller: caller };
            }
            else {
                console.error("重复注册相同事件:" + type + ",返回!");
            }
        }
        static removeEvent(type, caller, listener) {
            var listenerList = EventManager._evtMap[type];
            if (listenerList) {
                var len = listenerList.length;
                for (var i = len - 1; i >= 0; i--) {
                    var obj = listenerList[i];
                    var fun = obj.fun;
                    if (listener == fun && obj.caller == caller) {
                        listenerList.splice(i, 1);
                    }
                }
                if (listenerList.length == 0) {
                    delete EventManager._evtMap[type];
                }
            }
        }
        static hasTypeEvent(type) {
            return EventManager._evtMap.hasOwnProperty(type);
        }
        static hasEvent(type, caller, listener) {
            var listenerList = EventManager._evtMap[type];
            if (listenerList) {
                var len = listenerList.length;
                for (var i = len - 1; i >= 0; i--) {
                    var obj = listenerList[i];
                    var fun = obj.fun;
                    if (listener == fun && obj.caller == caller) {
                        return true;
                    }
                }
            }
            return false;
        }
        static removeAllEvents() {
            EventManager._evtMap = {};
        }
        static getTypeEventList(type) {
            var funList = new Array();
            var listenerList = EventManager._evtMap[type];
            if (listenerList) {
                var len = listenerList.length;
                for (var i = len - 1; i >= 0; i--) {
                    var obj = listenerList[i];
                    var fun = obj.fun;
                    funList.push(fun);
                }
            }
            return funList;
        }
        static getTypeEventsNum(type) {
            var list = EventManager._evtMap[type];
            return list ? list.length : 0;
        }
    }
    EventManager._evtMap = {};

    class MiniServer {
        constructor() {
            this._access_token = null;
        }
        get access_token() {
            return this._access_token;
        }
        setup(serverAddr) {
            this._serverAddr = serverAddr;
        }
        login() {
            var ex = new WxLogin();
            ex.login(this._serverAddr, new CallBackFunc(this, this.onGetTokensuccess), new CallBackFunc(this, this.onGetTokenFail));
        }
        regionLogin() {
            this.request('REGION_LOGIN', { cmdType: HttpServerCmd.REGION_LOGIN, }, null, 'post');
        }
        onGetTokenFail(args = null) {
            console.log("onGetTokenFail", args);
        }
        onGetTokensuccess(tokenRes) {
            console.log("tokenRes:", tokenRes);
            var data = tokenRes;
            if (data.code == 0) {
                var res = data.data;
                this._regionID = res.regionId;
                this._access_token = res.secretKey;
                this.regionLogin();
            }
        }
        updateServerInfo(res) {
            this._regionID = res.regionId;
            this._access_token = res.secretKey;
        }
        get isLoginSuccess() {
            return this._access_token != null;
        }
        request(api, data = null, isNeedRegion = true, method = "", heads = null) {
            var mini = new MiniHttpRequest();
            var url;
            if (isNeedRegion && this._regionID && this._regionID > 0) {
                url = this._serverAddr + "/s" + this._regionID;
            }
            else {
                url = this._serverAddr;
            }
            if ((typeof api === "string") && api.indexOf("https:") != -1) {
                url = api;
            }
            if (!method) {
                method = "GET";
                if (data) {
                    method = "POST";
                }
            }
            if (heads == null) {
                heads = { "X-WX-Skey": this._access_token };
            }
            else {
                heads["X-WX-Skey"] = this._access_token;
            }
            if (isNeedRegion) {
                heads[MiniServer.WX_HEADER_REGION] = this._regionID;
            }
            mini.request(url, method, data, heads);
            mini.on(Laya.Event.COMPLETE, this, this.onReqSuccess, [api]);
            mini.on(Laya.Event.ERROR, this, this.onReqError, [api]);
        }
        onReqSuccess(api, response) {
            if (response) {
                console.log('response', response);
                EventManager.dispatchEvent(api, response);
            }
        }
        onReqError(api, response) {
            if (response) {
                if (response.code == -2) ;
            }
        }
    }
    MiniServer.WX_HEADER_REGION = 'X-WX-Region';
    MiniServer.X_WX_GAME_ID = 'x-wx-game-id';
    MiniServer.X_WX_OPEN_ID = 'x-wx-open-id';

    class MiNi {
        constructor() {
        }
        static setup() {
            this._miniHttpSerer = new MiniServer();
            this._miniHttpSerer.setup('https://testcyorange.haidian666.com/minGame/cmd');
        }
        static login() {
            if (!this._miniHttpSerer) {
                console.error("Mini HTTP Server 尚未初始化");
                return;
            }
            this._miniHttpSerer.login();
        }
        static regionLogin() {
            this._miniHttpSerer.regionLogin();
        }
        static request(api, data = null, isNeedRegion = true, method = "", heads = null) {
            if (!this._miniHttpSerer) {
                console.error("Mini HTTP Server 尚未初始化");
                return;
            }
            this._miniHttpSerer.request(api, data, isNeedRegion, method, heads);
        }
    }

    class NoticeView extends ui.test.NoticeViewUI {
        static get ins() {
            if (this._instance == null) {
                this._instance = new NoticeView();
            }
            return this._instance;
        }
        constructor() {
            super();
            this.x = (Laya.stage.width - this.width) >> 1;
            this.y = ((Laya.stage.height - this.height) >> 1) + 100;
            this.zOrder = 1100;
        }
        show(text) {
            this.text.text = text;
            var width = Math.max(this.text.displayWidth + 40, 400);
            this.text.x = (this.width - this.text.displayWidth) >> 1;
            this.bg.width = width;
            this.bg.x = (this.width - width) >> 1;
            Laya.stage.addChild(this);
            Laya.timer.once(1500, this, this.hide);
            this.box.y = 0;
            Laya.Tween.to(this.box, { y: -50 }, 1000, null, null, 500, true);
        }
        hide() {
            this.removeSelf();
        }
    }

    class TimeUtil {
        static getTimeStr(time) {
            var data;
            if (time) {
                data = new Date(time);
            }
            else {
                data = new Date();
            }
            let month = Number(data.getMonth()) + 1;
            let str1 = month >= 10 ? month + '' : '0' + month;
            let date = data.getDate();
            let str2 = date >= 10 ? date + '' : '0' + date;
            return data.getFullYear() + "-" + str1 + "-" + str2;
        }
        static formatTime(time, fmt) {
            var o = {
                "M+": time.getMonth() + 1,
                "d+": time.getDate(),
                "h+": time.getHours(),
                "m+": time.getMinutes(),
                "s+": time.getSeconds(),
                "q+": Math.floor((time.getMonth() + 3) / 3),
                "S": time.getMilliseconds()
            };
            if (/(y+)/.test(fmt))
                fmt = fmt.replace(RegExp.$1, (time.getFullYear() + "").substr(4 - RegExp.$1.length));
            for (var k in o)
                if (new RegExp("(" + k + ")").test(fmt))
                    fmt = fmt.replace(RegExp.$1, (RegExp.$1.length == 1) ? (o[k]) : (("00" + o[k]).substr(("" + o[k]).length)));
            return fmt;
        }
        static getSecondTimeFomate(a) {
            var isShowHour = true;
            if (a < 3600) {
                isShowHour = false;
            }
            var h = Math.floor(a / 3600) + '';
            if (h.length == 1) {
                h = "0" + h;
            }
            var m = Math.floor((a % 3600) / 60) + '';
            if (m.length == 1) {
                m = "0" + m;
            }
            var s = Math.floor(a % 60) + '';
            if (s.length == 1) {
                s = "0" + s;
            }
            if (isShowHour) {
                return (h + ":" + m + ":" + s);
            }
            else {
                return (m + ":" + s);
            }
        }
        static getTimeStrFomate(time) {
            var str = "";
            var h = Math.floor(time / 3600);
            if (h > 0) {
                str += h + "小时";
            }
            var m = Math.floor((time % 3600) / 60);
            if (m > 0) {
                str += m + "分";
            }
            str += Math.floor(time % 60) + "秒";
            return str;
        }
        static getTimeFomate(time) {
            var str = "";
            var h = Math.floor(time / 3600);
            if (h > 0) {
                str += h + ":";
            }
            var m = Math.floor((time % 3600) / 60);
            if (m > 0) {
                str += m + ":";
            }
            str += Math.floor(time % 60);
            return str;
        }
        static checkIfPass1Day(curTime, lastTime) {
            if (curTime <= lastTime) {
                return false;
            }
            if (curTime.getFullYear() > lastTime.getFullYear()) {
                return true;
            }
            else if (curTime.getMonth() > lastTime.getMonth()) {
                return true;
            }
            else if (curTime.getDate() > lastTime.getDate()) {
                return true;
            }
            return false;
        }
    }

    class Dictionary {
        constructor() {
            this._keyValues = {};
        }
        set(key, value) {
            this._keyValues[key] = value;
        }
        get(key) {
            return this._keyValues[key];
        }
        remove(key) {
            this._keyValues[key] = null;
            return true;
        }
        clear() {
            this._keyValues = {};
        }
        get keys() {
            let ary = [];
            for (var key in this._keyValues) {
                ary.push(key);
            }
            return ary;
        }
    }

    class MsgLockManager {
        static isLock(msg) {
            return this._dic.get(msg);
        }
        static addLock(msg, time) {
            this._dic.set(msg, msg);
            Laya.timer.once(time, this, function () { MsgLockManager._dic.remove(msg); }, null, true);
        }
    }
    MsgLockManager._dic = new Dictionary();

    class OrderItemRender extends ui.test.OrderItemViewUI {
        constructor() {
            super();
            this.confer.on(Laya.Event.CLICK, this, this.onConfer);
        }
        onEnable() {
            if (!EventManager.hasEvent('SYNC_ORDER_STATUS', this, this.updateStatus)) {
                EventManager.addEvent('SYNC_ORDER_STATUS', this, this.updateStatus);
            }
        }
        onDisable() {
            EventManager.removeEvent('SYNC_ORDER_STATUS', this, this.updateStatus);
        }
        updateUi(data) {
            this.playername.text = data.realName;
            this.address.text = data.area + data.address;
            this.phone.text = data.phone + '';
            this.idnum.text = data.orderId ? data.orderId : '无';
            this.status.selectedIndex = data.status - 1;
            this.logincnt.text = data.loginCnt + '';
            this.exchangecnt.text = data.exchangeCnt + '';
            this.invitecnt.text = data.inviteCnt + '';
            this.submittime.text = TimeUtil.getTimeStr(data.submitTime);
            this.updateStatusString(data.status);
        }
        updateStatusString(status) {
            switch (status) {
                case 1:
                    this.statusstr.text = '审核中';
                    this.statusstr.color = '#ec3714';
                    break;
                case 2:
                    this.statusstr.text = '发货中';
                    this.statusstr.color = '#14ef0b';
                    break;
                case 3:
                    this.statusstr.text = '已发货';
                    this.statusstr.color = '#14ef0b';
                    break;
                case 4:
                    this.statusstr.text = '不通过';
                    this.statusstr.color = '#ec3714';
                    break;
            }
        }
        onConfer() {
            if (!MsgLockManager.isLock('SYNC_ORDER_STATUS')) {
                MiNi.request('SYNC_ORDER_STATUS', { cmdType: HttpServerCmd.SYNC_ORDER_STATUS, orderId: this.idnum.text, status: this.status.selectedIndex + 1 });
                MsgLockManager.addLock('SYNC_ORDER_STATUS', 200);
            }
        }
        updateStatus(res) {
            if (res && res.orderInfo && res.orderInfo.orderId === this.idnum.text) {
                if (res.orderInfo.status === this.status.selectedIndex + 1) {
                    NoticeView.ins.show('修改成功');
                    this.updateStatusString(this.status.selectedIndex + 1);
                }
                else {
                    NoticeView.ins.show('修改失败');
                }
            }
        }
    }

    class OrderInfo {
        get submitDate() {
            if (!this._submitDate) {
                this._submitDate = TimeUtil.getTimeStr(this.submitTime);
            }
            return this._submitDate;
        }
        setdata(data) {
            for (const key in data) {
                if (data.hasOwnProperty(key)) {
                    this[key] = data[key];
                }
            }
        }
    }

    class OrderListView extends ui.test.OrderListViewUI {
        constructor() {
            super();
            this.isModal = true;
            this.list.itemRender = OrderItemRender;
            this.list.vScrollBarSkin = '';
            this.list.renderHandler = new Laya.Handler(this, this.updateItem);
            this.list.spaceY = 10;
            this.list.scrollBar.elasticBackTime = 200;
            this.list.scrollBar.elasticDistance = 50;
            this.btn_search.on(Laya.Event.CLICK, this, this.onSearch);
            this.btn_selectdate.on(Laya.Event.CLICK, this, this.onSelectDate);
        }
        static get ins() {
            if (!this._ins) {
                this._ins = new OrderListView();
            }
            return this._ins;
        }
        onEnable() {
            this.updateUI();
            EventManager.addEvent('GET_ORDER_LIST', this, this.updateUI);
            MiNi.request('GET_ORDER_LIST', { cmdType: HttpServerCmd.GET_ORDER_LIST, openId: null, status: 0 });
        }
        onDisable() {
            EventManager.removeEvent('GET_ORDER_LIST', this, this.updateUI);
        }
        updateItem(cell, index) {
            cell.updateUi(this.orderList[index]);
        }
        updateUI(res) {
            if (res && res.orderList) {
                this.orderList = [];
                for (let data of res.orderList) {
                    let info = new OrderInfo();
                    info.setdata(data);
                    this.orderList.push(info);
                }
                this.list.array = res.orderList;
                if (this.orderList.length > 0) {
                    this.searchdate.text = this.orderList[0].submitDate;
                }
            }
            else {
                this.list.array = [];
            }
            this.nodata.visible = this.list.array.length === 0;
        }
        onSearch() {
            if (this.search.text) {
                for (let i = 0; i < this.orderList.length; i++) {
                    if (this.orderList[i].orderId === this.search.text || this.orderList[i].submitDate === this.search.text) {
                        this.list.scrollTo(i);
                        return;
                    }
                }
            }
        }
        onSelectDate() {
            if (this.searchdate.text) {
                if (!MsgLockManager.isLock('GET_ORDER_LIST')) {
                    MiNi.request('GET_ORDER_LIST', { cmdType: HttpServerCmd.GET_ORDER_LIST, openId: null, status: 0, date: this.searchdate.text });
                    MsgLockManager.addLock('GET_ORDER_LIST', 200);
                }
            }
        }
    }

    class GameEventListener {
        static init() {
            EventManager.addEvent('REGION_LOGIN', GameEventListener, GameEventListener.showListView);
        }
        static showListView() {
            OrderListView.ins.show();
        }
    }

    class GameUI extends ui.test.TestSceneUI {
        constructor() {
            super();
            GameUI.instance = this;
            this.login.on(Laya.Event.CLICK, this, this.onLogin);
            MiNi.setup();
            GameEventListener.init();
            EventManager.addEvent('ADMIN_LOGIN', this, this.onLoginSuccess);
        }
        onEnable() {
        }
        onLogin() {
            let min = new Laya.HttpRequest();
            let data = JSON.stringify({ cmdType: HttpServerCmd.ADMIN_LOGIN, username: this.username.text, password: this.password.text });
            min.send('https://testcyorange.haidian666.com/minGame/cmd', data, 'post', "json");
            min.on(Laya.Event.COMPLETE, this, this.onLoginSuccess);
            min.on(Laya.Event.ERROR, this, this.onLoginError);
        }
        onLoginSuccess(res) {
            if (res) {
                MiNi._miniHttpSerer.onGetTokensuccess(res);
            }
        }
        onLoginError(res) {
            console.log('错误', res);
        }
    }

    class GameConfig {
        constructor() { }
        static init() {
            var reg = Laya.ClassUtils.regClass;
            reg("script/GameUI.ts", GameUI);
        }
    }
    GameConfig.width = 1024;
    GameConfig.height = 768;
    GameConfig.scaleMode = "fixedheight";
    GameConfig.screenMode = "none";
    GameConfig.alignV = "top";
    GameConfig.alignH = "left";
    GameConfig.startScene = "test/TestScene.scene";
    GameConfig.sceneRoot = "";
    GameConfig.debug = false;
    GameConfig.stat = false;
    GameConfig.physicsDebug = false;
    GameConfig.exportSceneToJson = true;
    GameConfig.init();

    class Main {
        constructor() {
            if (window["Laya3D"])
                Laya3D.init(GameConfig.width, GameConfig.height);
            else
                Laya.init(GameConfig.width, GameConfig.height, Laya["WebGL"]);
            Laya["DebugPanel"] && Laya["DebugPanel"].enable();
            Laya.stage.scaleMode = GameConfig.scaleMode;
            Laya.stage.screenMode = GameConfig.screenMode;
            Laya.stage.alignV = GameConfig.alignV;
            Laya.stage.alignH = GameConfig.alignH;
            Laya.URL.exportSceneToJson = GameConfig.exportSceneToJson;
            if (GameConfig.debug || Laya.Utils.getQueryString("debug") == "true")
                Laya.enableDebugPanel();
            if (GameConfig.physicsDebug && Laya["PhysicsDebugDraw"])
                Laya["PhysicsDebugDraw"].enable();
            if (GameConfig.stat)
                Laya.Stat.show();
            Laya.alertGlobalError = true;
            Laya.loader.load(['res/atlas/comp.atlas', 'ui.json'], Laya.Handler.create(this, this.onResLoaded));
        }
        onResLoaded() {
            Laya.Scene.setUIMap("ui.json");
            GameConfig.startScene && Laya.Scene.open(GameConfig.startScene);
        }
    }
    new Main();

}());
